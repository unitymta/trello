import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ManageService {
	apiUrl= "http://127.0.0.1:8000";
	token = JSON.parse(localStorage.getItem("tokenLocal"));
	headers: HttpHeaders = new HttpHeaders({
		'Authorization': 'JWT ' + this.token,
		'Content-Type': 'application/json'
	});

	constructor(private http: HttpClient) { }

	getLists(): Observable<any[]> {
		return this.http.get<any[]>(`${this.apiUrl}/api-lists/lists`);
	}

	getCards(): Observable<any[]> {
		return this.http.get<any[]>(`${this.apiUrl}/api-cards/cards`);
	}

	updateListName(id: any, data: any): Observable<any> {
		return this.http.put<any>(`${this.apiUrl}/api-lists/lists/${id}`, data, { headers: this.headers });
	}

	addCard(data: any): Observable<any> {
		return this.http.post(`${this.apiUrl}/api-cards/cards/create`, data, { headers: this.headers });
	}

	deleteCard(id: any): Observable<any> {
		return this.http.delete(`${this.apiUrl}/api-cards/cards/${id}`, { headers: this.headers });
	}

	updateCardName(id: any, data: any): Observable<any> {
		return this.http.put(`${this.apiUrl}/api-cards/cards/${id}`, data, { headers: this.headers });
	}

	getComments(): Observable<any[]> {
		return this.http.get<any[]>(`${this.apiUrl}/api-comments/comments`);
	}

	addComment(data: any): Observable<any> {
		return this.http.post(`${this.apiUrl}/api-comments/comments/create`, data, { headers: this.headers });
	}
	
}
