import { Component, OnInit, Input } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
	selector: 'app-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
	apiHost = 'http://localhost:8000/api-token-auth/';	
	@Input() token;
	headers: HttpHeaders = new HttpHeaders({
		'Content-Type': 'application/json'
	});

	constructor(private http: HttpClient,
		private router: Router) { }

	ngOnInit() {
	}	

	auth() {
		let userLogin = document.getElementById('login');
		let userPassword = document.getElementById('password');

		if (userLogin['value'] !== '' && userPassword['value'] !== '') {	
			this.http.post(this.apiHost, {
				username: userLogin['value'],
				password: userPassword['value'],
				headers: this.headers
			}).subscribe(
				(data) => {
					if (data['token']) {
						this.token = data['token'];
						localStorage.setItem('tokenLocal', JSON.stringify(data['token']));
						// this.tokenChange.emit(this.token);
						this.router.navigate(['/boards']);
					}
				},
				error => {
					alert('Incorrect username or password. Please try again.');
				}
			);
		} else {
			alert('Please enter username and password.');
		}
	}

	logOut() {
		localStorage.removeItem('tokenLocal');
		window.location.reload();
	}
}
