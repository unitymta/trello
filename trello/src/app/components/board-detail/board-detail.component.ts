import { Component, OnInit } from '@angular/core';
import { ManageService } from '../../manage.service';

@Component({
	selector: 'app-board-detail',
	templateUrl: './board-detail.component.html',
	styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit {
	lists: any = [];
	cards: any = [];
	edit_lists = {
		name: "",
	}
	checkForm: 0;
	checkButton = true;
	checkOptionForList = 0;
	checkOptionForCard = 0;
	add_card = {
		list_id: 0,
		name: "",
		due_date: null,
		subscriber: true
	}

	constructor(private service: ManageService) { }

	ngOnInit() {
		this.getLists();
		this.getCards();
	}

	getLists(): void {
		this.service.getLists().subscribe(data => {
			this.lists = data;
		});
	}

	getCards(): void {
		this.service.getCards().subscribe(data => {
			this.cards = data;
		})
	}

	updateListName(id: any, event: any): void {
		let t = event.target.title.value;
		this.edit_lists['name'] = t;
		this.service.updateListName(id, this.edit_lists).subscribe();
	}

	addCard(list_id: any, event): void {
		let name = event.target.addCard.value;
		this.add_card['name'] = name;
		this.add_card['list_id'] = list_id;
		this.service.addCard(this.add_card).subscribe();
		this.getCards();
	}

	deleteCard(id: any): void {
		this.service.deleteCard(id).subscribe();
		this.getCards();
	}

	updateCardName(id: any, data: any, list_id: any): void {
		this.add_card['name'] = data;
		this.add_card['list_id'] = list_id;
		this.service.updateCardName(id, this.add_card).subscribe();
	}

	toggleAdd(event, id?: any): void {
		event.target.style.display = "none";
		this.checkForm = id;
		this.checkButton = false;
	}

	closeForm(): void {
		this.checkForm = 0;
		this.checkButton = true;
	}

	showOption(param1: any, param2: any): void {
		this.checkOptionForList = param1;
		this.checkOptionForCard = param2;
	}

	closeOption(): void {
		this.checkOptionForList = 0;
		this.checkOptionForCard = 0;
	}

}
