import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ManageService } from '../../manage.service';

@Component({
	selector: 'app-card',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
	commentValue = {
		card_id: 0,
		content: ""
	};
	commentList = [];

	constructor( private router: Router,
		private activeRoute: ActivatedRoute,
		private service: ManageService) { }

	ngOnInit() {
		this.getComments();
	}

	getComments(): void {
		let card_id = this.activeRoute.snapshot.paramMap.get('id');
		this.service.getComments().subscribe(data => {
			data.map(value => {
				if( card_id == value['card_id'] ){					
					this.commentList.push(value);
					console.log(this.commentList,'commneet')
				}
			})			
		});
	}

	saveComment(data: any): void {
		let card_id = this.activeRoute.snapshot.paramMap.get('id');
		this.commentValue['card_id'] = parseInt(card_id);
		this.commentValue['content'] = data;
		this.service.addComment(this.commentValue).subscribe();
		this.getComments();
	}

}
