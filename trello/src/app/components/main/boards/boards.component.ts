import { Component, OnInit, HostListener } from '@angular/core';

@Component({
	selector: 'app-boards',
	templateUrl: './boards.component.html',
	styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {
	stringUrl = "";
	checkBoard: boolean = false;
	
	@HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
		if(event.keyCode == 27){
			this.checkBoard = false;
		}
	}

	constructor() { }

	ngOnInit() {

	}

	makeid() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 15; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		this.stringUrl = text;
		return text;
	}

	createBoard() {
		this.checkBoard = !this.checkBoard;
	}

	closePopup() {
		this.checkBoard = !this.checkBoard;
	}

}
