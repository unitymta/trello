import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { HappyNewYearComponent } from './components/happy-new-year/happy-new-year.component';
import { HomeComponent } from './components/main/home/home.component';
import { BoardsComponent } from './components/main/boards/boards.component';
import { BoardDetailComponent } from './components/board-detail/board-detail.component';
import { AuthGuard } from '../app/authentication/auth.guard';
import { CardComponent } from './components/card/card.component';

const routes: Routes = [
	{ path: '', component: HappyNewYearComponent },
	{ path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
	{ path: 'boards', component: BoardsComponent, canActivate: [AuthGuard] },
	{ path: 'boards/:code', component: BoardDetailComponent, canActivate: [AuthGuard] },
	{ path: 'card/:id', component: CardComponent, canActivate: [AuthGuard] },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
