import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from "./app-material.module";
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/main/home/home.component';
import { BoardsComponent } from './components/main/boards/boards.component';
import { SidebarComponent } from './components/main/sidebar/sidebar.component';
import { BoardDetailComponent } from './components/board-detail/board-detail.component';
import { HappyNewYearComponent } from './components/happy-new-year/happy-new-year.component';
import { CardComponent } from './components/card/card.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthenticationComponent } from './authentication/authentication.component';

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		HomeComponent,
		BoardsComponent,
		SidebarComponent,
		BoardDetailComponent,
		HappyNewYearComponent,
		CardComponent,
		AuthenticationComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		AppMaterialModule,
		HttpClientModule,
		FormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
